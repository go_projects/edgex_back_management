package utils

import (
	cRand "crypto/rand"
	"fmt"
	"io"
	"math/rand"
	"strconv"
	"time"
)

func GenUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(cRand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	// variant bits; see section 4.1.1
	uuid[8] = uuid[8]&^0xc0 | 0x80
	// version 4 (pseudo-random); see section 4.1.3
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}

// 时间毫秒值 + 6位随机数
func GenRandomId() string {
	rand.Seed(time.Now().Unix())
	randomInt := rand.Intn(100000) + 100000
	return strconv.FormatInt(NowInMilli(), 10) + strconv.FormatInt(int64(randomInt), 10)
}

// 获取当前时间毫秒数
func NowInMilli() int64 {
	return time.Now().UnixNano() / 1e6
}

func TimestampToStr(timestp int64) string {
	//var t int64 = time.Now().Unix()
	timestp = timestp / 1000
	var s string = time.Unix(timestp, 0).Format("2006-01-02 15:04:05")
	return s
}
