package handlers

import (
	"edgex_back_management/dbops"
	"edgex_back_management/defs"
	"edgex_back_management/model"
	"edgex_back_management/utils"
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

func FindAllArticle(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	pageFrom := params.ByName("from")
	pageSize := params.ByName("size")

	if pageFrom == "" || pageSize == "" {
		pageFrom = "0"
		pageSize = "10"
	}

	from, err1 := strconv.Atoi(pageFrom)
	size, err2 := strconv.Atoi(pageSize)
	if err1 != nil || err2 != nil {
		log.Printf("error parse params: %v, %v", err1, err2)
		defs.SendErrorResponse(w, defs.ErrorInternalFaults)
		return
	}

	articles, err := dbops.FindAllArticle(from, size)
	for idx := range articles {
		articles[idx].CreatedStr = utils.TimestampToStr(articles[idx].Created)
	}

	if err != nil {
		log.Printf("error of find all article from db: %v", err)
		defs.SendErrorResponse(w, defs.ErrorDBError)
		return
	}

	tpl, err := template.ParseFiles("./templates/article_list.html")
	if err != nil {
		log.Printf("Error of parsing template article_list.html: %s", err)
		defs.SendErrorResponse(w, defs.ErrorInternalFaults)
		return
	}

	userName := r.Header.Get(HeaderOfUserName)
	page := model.ArticlesPage{UserName: userName, Articles: articles}
	tpl.Execute(w, page)
	return

	if result, err := json.Marshal(articles); err != nil {
		defs.SendErrorResponse(w, defs.ErrorInternalFaults)
		return
	} else {
		defs.SendSuccessResponse(w, string(result), 200)
	}
}

func GetArticle(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	articleId := params.ByName("id")
	article, err := dbops.GetArticle(articleId)
	if err != nil {
		defs.SendErrorResponse(w, defs.ErrorDBError)
		return
	}

	tpl, err := template.ParseFiles("./templates/article_detail.html")
	if err != nil {
		log.Printf("Error of parsing template login.html: %s", err)
		defs.SendErrorResponse(w, defs.ErrorInternalFaults)
		return
	}

	userName := r.Header.Get(HeaderOfUserName)
	page := model.ArticlePage{UserName: userName, Article: *article}
	tpl.Execute(w, page)
	return

	if result, err := json.Marshal(article); err != nil {
		defs.SendErrorResponse(w, defs.ErrorInternalFaults)
		return
	} else {
		defs.SendSuccessResponse(w, string(result), 200)
	}
}

func PassArticle(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	articleId := params.ByName("id")
	ok := dbops.PassArticle(articleId)
	if !ok {
		defs.SendErrorResponse(w, defs.ErrorInternalFaults)
	}

	defs.SendSuccessResponse(w, "ok!", 200)
}

func RejectArticle(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	articleId := params.ByName("id")
	ok := dbops.RejectArticle(articleId)
	if !ok {
		defs.SendErrorResponse(w, defs.ErrorInternalFaults)
	}

	defs.SendSuccessResponse(w, "ok", 200)
}

func DeleteArticle(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	articleId := params.ByName("id")
	ok := dbops.DeleteArticle(articleId)
	if !ok {
		defs.SendErrorResponse(w, defs.ErrorInternalFaults)
	}
	defs.SendSuccessResponse(w, "ok", 200)
}
