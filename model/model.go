package model

import "gopkg.in/mgo.v2/bson"

type SignedUp struct {
	Success   string `json:"success"`
	SessionId string `json:"sessionId"`
}

type Logout struct {
	Success  string `json:"success"`
	UserName string `json:"userName"`
	Message  string `json:"message"`
}

type HomePage struct {
	Name string
}

type ArticlesPage struct {
	UserName string
	Articles []Article
}

type ArticlePage struct {
	UserName string
	Article  Article
}

type SimpleUser struct {
	UserName  string `bson:"name" json:"userName"`
	AvatarUrl string `bson:"avatarUrl" json:"avatarUrl"`
}

type User struct {
	UserName  string `bson:"name" json:"userName"`
	Password  string `json:"password"`
	AvatarUrl string `bson:"avatarUrl" json:"avatarUrl"`
}

type Session struct {
	//Id       bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Id       string `bson:"_id,omitempty" json:"id"`
	UserName string `bson:"userName"   json:"userName"`
	// time to live
	TTL int64 `bson:"ttl" json:"ttl"`
}

type Article struct {
	Id         bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Title      string        `bson:"title"    json:"title"`
	Content    string        `bson:"content"  json:"content"`
	Type       string        `bson:"type"     json:"type"`
	UserName   string        `bson:"userName" json:"userName"`
	UserId     string        `bson:"userId"   json:"userId"`
	AvatarUrl  string        `bson:"avatarUrl"     json:"avatarUrl"`
	Posted     bool          `bson:"posted"   json:"posted"`
	Approved   bool          `bson:"approved"   json:"approved"`
	ReadCount  int64         `bson:"readCount" json:"readCount"`
	Created    int64         `bson:"created"  json:"created"`
	CreatedStr string        `bson:"createdStr"  json:"createdStr"`
	Modified   int64         `bson:"modified" json:"modified"`
}
