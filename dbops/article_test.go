package dbops

import (
	"fmt"
	"testing"
)

func TestArticleOps(t *testing.T) {

}

func TestFindAllArticle(t *testing.T) {
	articles, err := FindAllArticle(0, 100)
	if err != nil {
		t.Errorf("Error of test find all article: %v", err)
	}

	fmt.Println(len(articles))
}

func TestGetArticle(t *testing.T) {
	article, err := GetArticle("5bc0ad47cedad52c2d5e21c6")
	if err != nil {
		t.Errorf("Error of test find one article: %v", err)
	}

	fmt.Println(article.UserId)
}

func TestPassArticle(t *testing.T) {
	ok := PassArticle("5bc0ad47cedad52c2d5e21c6")
	if !ok {
		t.Errorf("Error of test pass article")
	}
}

func TestRejectArticle(t *testing.T) {
	ok := RejectArticle("5bc0ad47cedad52c2d5e21c6")
	if !ok {
		t.Errorf("Error of test pass article")
	}
}

func TestDeleteArticle(t *testing.T) {
	ok := DeleteArticle("5bc0ad47cedad52c2d5e21c6")
	if !ok {
		t.Errorf("Error of test delete article")
	}
}
