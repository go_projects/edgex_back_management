package dbops

import (
	"edgex_back_management/utils"
	"fmt"
	"testing"
)

func TestMain(m *testing.M) {
	m.Run()
}

func TestSessionOps(t *testing.T) {
	t.Run("Session: save()", TestSave)
	t.Run("Session: findOne()", TestFindOne)
	t.Run("Session: findAll()", TestFindAll)
	t.Run("Session: deleteOne()", TestDeleteOne)
}

func TestSave(t *testing.T) {
	sessionId := utils.GenRandomId()
	err := SaveSession(sessionId, 2000, "thank")
	if err != nil {
		t.Errorf("ErrorMsg of save session: %v", err)
	}
}

func TestFindOne(t *testing.T) {
	sessionId := "1541146953766171988"
	session, err := FindOneSessionById(sessionId)
	if err != nil {
		t.Errorf("ErrorMsg of find one session: %v", err)
	}
	fmt.Println("Result:", session)
}

func TestFindAll(t *testing.T) {
	allMap, err := FindAllSession()
	if err != nil {
		t.Errorf("ErrorMsg of find all session: %v", err)
	}
	fmt.Println("Result:", allMap)
	//allMap.Range(func(key, value interface{}) bool {
	//	fmt.Println("key:", key, "value:", value)
	//	return true
	//})
}

func TestDeleteOne(t *testing.T) {
	sessionId := "5bdbf4b20a6244c6d7b82797"
	err := DeleteOneSession(sessionId)
	if err != nil {
		t.Errorf("ErrorMsg of delete one session: %v", err)
	}
}

func TestRandom(t *testing.T) {
	fmt.Println(utils.GenRandomId())
}
