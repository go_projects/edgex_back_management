package dbops

import (
	"gopkg.in/mgo.v2"
	"log"
	"time"
)

type DataStore struct {
	S *mgo.Session
}

var (
	DS DataStore
)

const (
	HUAQIAO_ADDR = "39.98.39.186:8500"
	FAYANG_ADDR  = "47.94.210.157:8500"
)

func (ds DataStore) DataStore() *DataStore {
	return &DataStore{ds.S.Copy()}
}

func init() {
	log.Printf("-----------MongoDBConnect-------------")
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs: []string{FAYANG_ADDR},
		//Addrs:    []string{"192.168.100.214:27017"},
		Timeout:  time.Duration(5000) * time.Millisecond,
		Database: "edgex-club",
		Username: "",
		Password: "",
	}
	s, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		panic("db conn error")
	}
	s.SetSocketTimeout(time.Duration(5000) * time.Millisecond)
	DS.S = s
}
