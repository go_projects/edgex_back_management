package dbops

import (
	"edgex_back_management/model"
	"gopkg.in/mgo.v2/bson"
)

type UserRepository struct {
}

var UserRepos = &UserRepository{}

func FindUserByName(userName string) (model.User, error) {
	store := DS.DataStore()
	defer store.S.Close()

	user := model.User{}
	err := store.S.DB("edgex-club").C("user").Find(bson.M{"name": userName}).One(&user)

	if err != nil {
		return user, err
	}

	return user, nil
}
